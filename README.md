**AntiHackBot**

A Discord Bot for the Discord Hack Week made by engel_pika#3979 (176335473977851905).

This is just the basic/beginning of a Bot. One week is a very short time, especially if you have a lot other things todo.
That's why I decided to record how long I work on this Bot/Project (See below).

As I don't have so much time for this project I will keep it simple by using silly JSON files for language & database (So everyone can run the Bot without any knowledge) :P
But there is no need to host the Bot yourself, as I will run a version on my server.
Invite the Bot: [AntiHackBot Invite - Select](https://discordapp.com/oauth2/authorize?client_id=594097441180090398&permissions=1312156993&scope=bot)
Additional I'm trying a new kind of file sorting & function handling for myself. So either it works better or it doesn't :D

And don't hate my code, I'm sometimes really silly xD
But I always test every feature and only release things which are working.
My last bugs in other project were things like a "unknown string" - I forgot a 's' in the stringCode :P

__Bot FAQ & Features/Commands__
*Coming as soon as I created a command :P*
Commands:
* ah!ping
* none

__Task List__
* [ ] Normal Moderation Commands
* [ ] Multiple Languages (provide support - not to have the translations itself)
* [ ] First Version running '24/7'
* [ ] Server Wide Warnings & DM notification
* [ ] AntiRaid - Filter
* [ ] AntiRaid - Spam


__Time table (Weekdays in German timezone)__
*     Time - Day & Action
* 0h16m11s - Thursday evening, project setup (Sadly that I had todo that again lul, duo a little conflict).
* 0h39m29s - Friday morning, project setup (lol, again^^) & Hello World
* 0h40m42s - Friday noon, Working on the Bot
* 1h50m18s - Friday afternoon, Working on the Bot (Defining Base Scripts)
* 0h38m56s - Friday afternoon-evening, Working on the Bot
Total: 4h5m42s :D
